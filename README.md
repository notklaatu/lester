# Fat Chance, Lester

This is a repository managing the website assets for the band Fat Chance Lester. This does not host the music by the band.

The original code for the site is by my pal Snack_Machine_B.

I chopped it all up and adapted it for the band's site.


## Is this useful?

This is probably not terribly useful to you, whomever you are. It's just a bunch of random html and css and js files for a weird rock band's site. You're welcome to use it, but I'm just warning you that it's not, like, you know, a code release. It's just a random stash of code on the internet.